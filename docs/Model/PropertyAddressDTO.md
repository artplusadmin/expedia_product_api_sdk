# PropertyAddressDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city** | **string** | City in which the property is located | [optional] 
**country_code** | **string** | ISO 3166-1 Alpha 3 country code, for the country where the property is located | [optional] 
**line1** | **string** | First line of address | [optional] 
**line2** | **string** | Second line of address, not always available | [optional] 
**postal_code** | **string** | Postal or State Code, might not be available | [optional] 
**state** | **string** | State/Province, which is optional and thus might not be available | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


