# SurchargeDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | The type of surcharge applied to the extra bed. | 
**amount** | **double** | If the surcharge type is different than &#39;Free&#39;, must be greater than 0. Otherwise, must not be defined. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


