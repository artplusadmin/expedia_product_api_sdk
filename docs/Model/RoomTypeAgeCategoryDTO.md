# RoomTypeAgeCategoryDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **string** | The age category whose occupancy is being defined | [optional] 
**min_age** | **int** | Minimum age allowed for the category. Min 0, max 99 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


