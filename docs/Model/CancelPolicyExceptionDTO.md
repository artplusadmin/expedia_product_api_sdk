# CancelPolicyExceptionDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**end_date** | **string** | Format: YYYY-MM-DD. Cancel penalty exception&#39;s end date | [optional] 
**penalties** | [**\Swagger\Client\Model\PenaltyDTO[]**](PenaltyDTO.md) | Definition of the exception penalties applied | [optional] 
**start_date** | **string** | Format: YYYY-MM-DD. Cancel penalty exception&#39;s starting date | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


