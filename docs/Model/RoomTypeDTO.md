# RoomTypeDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**age_categories** | [**\Swagger\Client\Model\RoomTypeAgeCategoryDTO[]**](RoomTypeAgeCategoryDTO.md) | Defines the different age categories supported by the room type. At the very least, the &#39;Adult&#39; category must be defined. | 
**extra_bedding** | [**\Swagger\Client\Model\ExtraBedDTO[]**](ExtraBedDTO.md) | Defines the configuration (type, size and quality) of any extra beds the room type may have. | 
**max_occupancy** | [**\Swagger\Client\Model\OccupancyDTO**](OccupancyDTO.md) | Defines the maximum occupancy for the room, counting adults and children. | 
**name** | [**\Swagger\Client\Model\RoomTypeNameDTO**](RoomTypeNameDTO.md) | Formed by a [ISO 639-1]-[ISO-3166-1]. If applicable, it contains the RNS attributes used to generate the value. Max 255 characters | 
**partner_code** | **string** | Partner room type code/identifier. Max. 40 characters | 
**resource_id** | **int** | Expedia ID for this resource. Generated when created. Generated on POST, required on PUT | [optional] 
**room_size** | [**\Swagger\Client\Model\RoomSizeDTO**](RoomSizeDTO.md) | Used to define room size. When used, both size in square feet and in square meters must be specified. | [optional] 
**smoking_preferences** | **string[]** | Used to define whether the room type is smoking, nonsmoking, or if both options are available on request. If a single smoking option is provided, then the room is, by default, only available in this configuration. If both options are provided, then a choice will be offered to the customer at the time he makes a reservation, and the customer preference will be sent in electronic booking messages to the partner | 
**standard_bedding** | [**\Swagger\Client\Model\BeddingOptionDTO[]**](BeddingOptionDTO.md) | Lists the different combination of beds (type, size and quality) a room may have. | 
**status** | **string** | Room type status is derived from the rate plans under the room type: if at least 1 rate plan is active, the room type status will be active. If all rate plans are inactive, then the room type becomes inactive as well. | [optional] 
**views** | **string[]** | Used to indicate the views available from this room. | [optional] 
**wheelchair_accessibility** | **bool** | Used to indicate whether the room is configured to be wheelchair accessible or not. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


