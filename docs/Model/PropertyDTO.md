# PropertyDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | [**\Swagger\Client\Model\PropertyAddressDTO**](PropertyAddressDTO.md) | Property address details | [optional] 
**base_allocation_enabled** | **bool** | Boolean to indicate whether this property has a base allocation contract with Expedia. | [optional] 
**cancellation_time** | **string** | Cancellation deadline reference time. When cancel policies are defined and exchanged via the rate plan resource, a deadline in hours is provided. The deadline in hours is always relative to this property cancellation deadline reference time configuration | [optional] 
**currency** | **string** | Format: ISO 4217 Alpha 3. This currency code is applicable to all amounts found in any resources available as part of the EPS Product API. | [optional] 
**distribution_models** | **string[]** | Distribution model(s) under which the property is configured to work with Expedia | [optional] 
**min_los_threshold** | **int** | This property configuration is used by Expedia when MinLOS Restrictions updates are received via EQC AR. If the MinLOS restriction update attempted via EQC AR is greater than this value, the update will be rejected. | [optional] 
**name** | **string** | Name describing the property. Max. 255 characters | [optional] 
**partner_code** | **string** | Partner property code/identifier. Max 64 characters. Optional field returned for connected properties. This code allows partner to uniquely identify the property in its system. | [optional] 
**pricing_model** | **string** | Configuration of the property when it comes to pricing rooms and rates. | [optional] 
**rate_acquisition_type** | **string** | Describes which type of rate will be provided via this API, but also which type of rate should be used when managing availability and rates in  ExpediaPartnerCentral or using EC or EQC APIs. | [optional] 
**reservation_cut_off** | [**\Swagger\Client\Model\CutOffDTO**](CutOffDTO.md) | Used to indicate when we stop making rate plans available to book for same day reservations. | [optional] 
**resource_id** | **int** | Expedia ID for this resource. Generated when created. Generated on POST, required on PUT | [optional] 
**status** | **string** | Status in which the property can be in; Allowed values are: Active, Inactive, Onboarding, UnderConversion | [optional] 
**tax_inclusive** | **bool** | Returned to indicate whether the rate being exchanged over other APIs (availability/rates or booking) is inclusive of taxes or not. | [optional] 
**timezone** | **string** | Descriptive information about property timezone configuration in Expedia system. Description will start by a GMT offset, followed by a friendly name. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


