# CancelPolicyDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**default_penalties** | [**\Swagger\Client\Model\PenaltyDTO[]**](PenaltyDTO.md) | Default penalties&#39; definition. Min 1, Max 2 penalties defined | [optional] 
**exceptions** | [**\Swagger\Client\Model\CancelPolicyExceptionDTO[]**](CancelPolicyExceptionDTO.md) | List of exceptional cancel penalties defined for the Rate Plan | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


