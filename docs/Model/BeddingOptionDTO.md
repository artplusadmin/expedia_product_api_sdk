# BeddingOptionDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**option** | [**\Swagger\Client\Model\BedDTO[]**](BedDTO.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


