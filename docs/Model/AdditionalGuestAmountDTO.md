# AdditionalGuestAmountDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**age_category** | **string** | The age category for the additional guests | [optional] 
**amount** | **double** | Min value 0.000, accepts up to 3 decimal points | [optional] 
**date_end** | **string** | Date at which this amount will not be effective anymore. If no end date defined, will be returned as 2079-06-06. | [optional] 
**date_start** | **string** | Date at which this amount started being applicable, can be in the past | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


