# ResponseWrapperDTOListRatePlanDTO_

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entity** | [**\Swagger\Client\Model\RatePlanDTO[]**](RatePlanDTO.md) |  | [optional] 
**errors** | [**\Swagger\Client\Model\ErrorDTO[]**](ErrorDTO.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


