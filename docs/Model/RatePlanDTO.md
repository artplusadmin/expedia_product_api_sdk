# RatePlanDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**additional_guest_amounts** | [**\Swagger\Client\Model\AdditionalGuestAmountDTO[]**](AdditionalGuestAmountDTO.md) | Array of additional guest amounts. Up to 6 can be specified, 1 per category. Only 1 amount can be given per category, for all dates | [optional] 
**book_date_end** | **string** | Date at which this rate plan stops being available for searching on any Expedia POS. Format YYYY-MM-DD. If not restricted, will be returned as 2079-06-06. If in 2079, indicates this rate plan book end date is unrestricted | [optional] 
**book_date_start** | **string** | Date at which this rate plan starts being available for searching on any Expedia POS. If in the past, indicates rate plan book date start is not restricted. Accepted format: YYYY-MM-DD. If not restricted, will be returned as 1900-01-01 | [optional] 
**cancel_policy** | [**\Swagger\Client\Model\CancelPolicyDTO**](CancelPolicyDTO.md) | Default cancel policy. If not provided in a create request, the product API will select a refundable cancellation policy that is currently used by the most recently created standalone rate plan under the same property | [optional] 
**distribution_rules** | [**\Swagger\Client\Model\DistributionRuleDTO[]**](DistributionRuleDTO.md) | Used to provide information about how this rate plan can be sold (ExpediaCollect, HotelCollect or both). | 
**max_adv_book_days** | **int** | The maximum days before a stay date that the rate plan can be sold. Min 1, Max 500 | [optional] 
**max_los_default** | **int** | Default maximum LengthOfStay restriction. Min 1, Max 28. Set to 28 by default if not provided in a create request. Will always be considered along the value defined for each stay date, and the most restrictive of this default and the daily restriction will prevail | [optional] 
**min_adv_book_days** | **int** | The minimum days before a stay date that the rate plan can be sold. Min 1, Max 500 | [optional] 
**min_los_default** | **int** | Default minimum LengthOfStay restriction. Min 1, Max 28. Set to 1 by default if not provided in a create request. Will always be considered along the value defined for each stay date, and the most restrictive of this default and the daily restriction will prevail | [optional] 
**mobile_only** | **bool** | Indicates this rate plan is only available through shopping done on mobile devices | [optional] 
**name** | **string** | Name of the rate plan, for information/identification purposes. Min 1, Max 40 characters. If not provided, defaults to the manageable rate plan partner code. | 
**occupants_for_base_rate** | **int** | Max occupants allowed for the base rate. Min 1, Max 20. This is only applicable for per day pricing properties, and required in create requests. It indicates how many occupants the per day price applies to | 
**pricing_model** | **string** | Rate Plan pricing model. Will default to property’s pricing model, and if provided, it has to match the property’s pricing model | [optional] 
**rate_acquisition_type** | **string** | Rate acquisition type, inherited from the Property | [optional] 
**resource_id** | **int** | Expedia ID for this resource. Generated when created. Generated on POST, required on PUT | [optional] 
**status** | **string** | Defaults to active if not provided during creation | [optional] 
**tax_inclusive** | **bool** | Returned to indicate whether the rate being exchanged over other APIs (availability/rates or booking) is inclusive of taxes or not. During creation, for properties managing net rates, the default value is false. For sell rates, it is based on the property&#39;s configuration | [optional] 
**travel_date_end** | **string** | Latest date at which customers can checkout for a stay including this rate plan. Format YYYY-MM-DD. If not restricted, will be returned as 2079-06-06. If in 2079, indicates rate plan travel end date is not restricted | [optional] 
**travel_date_start** | **string** | Date at which customers can start checking in for a stay including this rate plan. Format YYYY-MM-DD. If not restricted, will be returned at 1900-01-01.If in the past, indicates rate plan travel start date is not restricted | [optional] 
**type** | **string** | Rate Plan type. Only Standalone type can be managed through the Product API. Defaults to Standalone if not provided during creation | [optional] 
**value_add_inclusions** | **string[]** | Array of value add inclusions. Value add inclusions are special features included with this rate. Breakfast, Internet, or parking inclusions are the most frequently used ones | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


