# CompensationRuleDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**exceptions** | [**\Swagger\Client\Model\CompensationExceptionRuleDTO[]**](CompensationExceptionRuleDTO.md) | Depending on the contractual agreement between Expedia and the partner, compensation can vary based on different criteria. This array of exceptions will reflect this | [optional] 
**min_amount** | **double** | Minimum amount. Accepts up to 3 decimal points. Only applicable to ExpediaCollect distribution rules | 
**percent** | **double** | Compensation percentage applied by default. Expressed as a value. Between 0 and 1, accepts up to 4 decimal points | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


