# CompensationExceptionRuleDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date_end** | **string** | End date of the exception rule. Accepted format: YYYY-MM-DD | [optional] 
**date_start** | **string** | Starting date of the exception rule. Accepted format: YYYY-MM-DD | [optional] 
**fri** | **bool** | For any exception, all 7 days of week are returned with a true/false indicator. | [optional] 
**min_amount** | **double** | Accepts up to 3 decimal points | [optional] 
**mon** | **bool** | For any exception, all 7 days of week are returned with a true/false indicator. | [optional] 
**percent** | **double** | Between 0 and 1, accepts up to 3 decimal points | [optional] 
**sat** | **bool** | For any exception, all 7 days of week are returned with a true/false indicator. | [optional] 
**sun** | **bool** | For any exception, all 7 days of week are returned with a true/false indicator. | [optional] 
**thu** | **bool** | For any exception, all 7 days of week are returned with a true/false indicator. | [optional] 
**tue** | **bool** | For any exception, all 7 days of week are returned with a true/false indicator. | [optional] 
**wed** | **bool** | For any exception, all 7 days of week are returned with a true/false indicator. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


