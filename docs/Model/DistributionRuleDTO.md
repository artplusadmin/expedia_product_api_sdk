# DistributionRuleDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**compensation** | [**\Swagger\Client\Model\CompensationRuleDTO**](CompensationRuleDTO.md) | Applicable compensation rules for this distribution model. Defaults to the value defined on the contract with the partner | [optional] 
**distribution_model** | **string** | Distribution model adopted by the rate plan, matching property configuration. ExpediaCollect: indicates that when customers book this rate plan, they will pay Expedia. HotelCollect: indicates that when customers book this rate plan, they will pay the property. Note: if a product can be sold as both ExpediaCollect and HotelCollect, there will be 2 distribution rules under the rate plan to indicate this | 
**expedia_id** | **string** | String, min 1, max 50 characters. Expedia rate plan ID that will be specified in booking messages and that should be used to manage avail/rates if this set of distribution rules is marked as manageable. | [optional] 
**manageable** | **bool** | Cannot be provided in a create request. Default to yes for HotelCollect-only or ExpediaCollect-only rate plans. For ExpediaTravelerPreference rate plans, if rate acquisition type is net, ExpediaCollect will default to true; if rate acquisition type is Sell/LAR, HotelCollect will default to true. | [optional] 
**partner_code** | **string** | Unique partner identifier for the rate plan. For a given room type, this code has to be unique per distribution model (e.g. for all ExpediaCollect rate plan distribution rules under this room, this code has to be unique). Uniqueness will be validated by Expedia during create or update operations. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


