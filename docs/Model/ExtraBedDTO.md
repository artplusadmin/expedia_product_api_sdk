# ExtraBedDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**quantity** | **int** |  | [optional] 
**size** | **string** |  | [optional] 
**surcharge** | [**\Swagger\Client\Model\SurchargeDTO**](SurchargeDTO.md) | Surcharge applied to the extra bed. | [optional] 
**type** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


