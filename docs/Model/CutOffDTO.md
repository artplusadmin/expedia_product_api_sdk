# CutOffDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**day** | **string** | Can be of same day or next day. Complements the time attribute | [optional] 
**time** | **string** | Indicates at which time we’ll stop making inventory available for same day reservations | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


