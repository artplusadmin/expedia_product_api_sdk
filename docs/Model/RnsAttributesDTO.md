# RnsAttributesDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessibility** | **bool** | Attribute that determines if room is considered wheelchair accessible | [optional] 
**area** | **string** | Attributed used to highlight the location of the room | [optional] 
**bedroom_details** | **string** | Attribute that describes details of the bedroom used to compose the name of the room | [optional] 
**custom_label** | **string** | Free text that can be appended to the name generated by the attributes. Use of this attribute is discouraged (see full spec). Max 37 characters | [optional] 
**featured_amenity** | **string** | Attribute used to highlight a feature of the room on its name | [optional] 
**include_bed_type** | **bool** | Attribute that determines whether or not to include bed type on the room name | [optional] 
**include_smoking_pref** | **bool** | Attribute that determines if room has smoking preference | [optional] 
**room_class** | **string** | Attribute that described the class of room, which is used to compose the name | [optional] 
**type_of_room** | **string** | Attribute that determines the type of room, which is used to compose the name | [optional] 
**view** | **string** | Attribute that gives additional information about the view of the room | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


