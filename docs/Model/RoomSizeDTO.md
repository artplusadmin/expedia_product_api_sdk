# RoomSizeDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**square_feet** | **int** | Room size in square feet. No decimals. | 
**square_meters** | **int** | Room size in square meters. No decimals. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


