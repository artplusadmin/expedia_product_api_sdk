# RoomTypeNameDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attributes** | [**\Swagger\Client\Model\RnsAttributesDTO**](RnsAttributesDTO.md) | Defines the attributes used to compose the common name for the room. You should not provide a custom name when using these attributes | [optional] 
**value** | **string** | The custom name provided for the room. Max. 255 characters. Not to be used in conjunction with the attributes | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


