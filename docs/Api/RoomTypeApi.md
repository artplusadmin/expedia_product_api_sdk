# Swagger\Client\RoomTypeApi

All URIs are relative to *https://services.expediapartnercentral.com/products*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createRoomType**](RoomTypeApi.md#createRoomType) | **POST** /properties/{propertyId}/roomTypes | Creates a new room type
[**getRoomType**](RoomTypeApi.md#getRoomType) | **GET** /properties/{propertyId}/roomTypes/{roomTypeId} | Read a single room type from a given property
[**getRoomTypes**](RoomTypeApi.md#getRoomTypes) | **GET** /properties/{propertyId}/roomTypes | Obtain a list of room types for a given property
[**patchRoomType**](RoomTypeApi.md#patchRoomType) | **PATCH** /properties/{propertyId}/roomTypes/{roomTypeId} | Patch an existing room type
[**updateRoomType**](RoomTypeApi.md#updateRoomType) | **PUT** /properties/{propertyId}/roomTypes/{roomTypeId} | Modify an existing room type


# **createRoomType**
> \Swagger\Client\Model\ResponseWrapperDTORoomTypeDTO_ createRoomType($property_id, $room_type_dto)

Creates a new room type

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');

$api_instance = new Swagger\Client\Api\RoomTypeApi();
$property_id = "property_id_example"; // string | Expedia Property Id
$room_type_dto = new \Swagger\Client\Model\RoomTypeDTO(); // \Swagger\Client\Model\RoomTypeDTO | JSON message describing the new room type

try {
    $result = $api_instance->createRoomType($property_id, $room_type_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoomTypeApi->createRoomType: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **string**| Expedia Property Id |
 **room_type_dto** | [**\Swagger\Client\Model\RoomTypeDTO**](../Model/\Swagger\Client\Model\RoomTypeDTO.md)| JSON message describing the new room type |

### Return type

[**\Swagger\Client\Model\ResponseWrapperDTORoomTypeDTO_**](../Model/ResponseWrapperDTORoomTypeDTO_.md)

### Authorization

[Basic](../../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/vnd.expedia.eps.product-v2+json
 - **Accept**: application/vnd.expedia.eps.product-v2+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getRoomType**
> \Swagger\Client\Model\ResponseWrapperDTORoomTypeDTO_ getRoomType($property_id, $room_type_id)

Read a single room type from a given property

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');

$api_instance = new Swagger\Client\Api\RoomTypeApi();
$property_id = "property_id_example"; // string | Expedia Property ID
$room_type_id = "room_type_id_example"; // string | Room type resource ID. Integer

try {
    $result = $api_instance->getRoomType($property_id, $room_type_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoomTypeApi->getRoomType: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **string**| Expedia Property ID |
 **room_type_id** | **string**| Room type resource ID. Integer |

### Return type

[**\Swagger\Client\Model\ResponseWrapperDTORoomTypeDTO_**](../Model/ResponseWrapperDTORoomTypeDTO_.md)

### Authorization

[Basic](../../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.expedia.eps.product-v2+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getRoomTypes**
> \Swagger\Client\Model\ResponseWrapperDTOListRoomTypeDTO_ getRoomTypes($property_id, $status)

Obtain a list of room types for a given property

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');

$api_instance = new Swagger\Client\Api\RoomTypeApi();
$property_id = "property_id_example"; // string | Expedia Property ID
$status = "status_example"; // string | Status filter. Only supported value is \"all\".

try {
    $result = $api_instance->getRoomTypes($property_id, $status);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoomTypeApi->getRoomTypes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **string**| Expedia Property ID |
 **status** | **string**| Status filter. Only supported value is \&quot;all\&quot;. | [optional]

### Return type

[**\Swagger\Client\Model\ResponseWrapperDTOListRoomTypeDTO_**](../Model/ResponseWrapperDTOListRoomTypeDTO_.md)

### Authorization

[Basic](../../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.expedia.eps.product-v2+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **patchRoomType**
> \Swagger\Client\Model\ResponseWrapperDTORoomTypeDTO_ patchRoomType($property_id, $room_type_id, $room_type)

Patch an existing room type

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');

$api_instance = new Swagger\Client\Api\RoomTypeApi();
$property_id = "property_id_example"; // string | Expedia Property Id
$room_type_id = "room_type_id_example"; // string | Room type resource ID
$room_type = "room_type_example"; // string | JSON message of partially updated room type

try {
    $result = $api_instance->patchRoomType($property_id, $room_type_id, $room_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoomTypeApi->patchRoomType: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **string**| Expedia Property Id |
 **room_type_id** | **string**| Room type resource ID |
 **room_type** | **string**| JSON message of partially updated room type |

### Return type

[**\Swagger\Client\Model\ResponseWrapperDTORoomTypeDTO_**](../Model/ResponseWrapperDTORoomTypeDTO_.md)

### Authorization

[Basic](../../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/vnd.expedia.eps.product-v2+json
 - **Accept**: application/vnd.expedia.eps.product-v2+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateRoomType**
> \Swagger\Client\Model\ResponseWrapperDTORoomTypeDTO_ updateRoomType($property_id, $room_type_id, $room_type_dto)

Modify an existing room type

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');

$api_instance = new Swagger\Client\Api\RoomTypeApi();
$property_id = "property_id_example"; // string | Expedia Property Id
$room_type_id = "room_type_id_example"; // string | Room type resource ID
$room_type_dto = new \Swagger\Client\Model\RoomTypeDTO(); // \Swagger\Client\Model\RoomTypeDTO | JSON message with modified room type

try {
    $result = $api_instance->updateRoomType($property_id, $room_type_id, $room_type_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoomTypeApi->updateRoomType: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **string**| Expedia Property Id |
 **room_type_id** | **string**| Room type resource ID |
 **room_type_dto** | [**\Swagger\Client\Model\RoomTypeDTO**](../Model/\Swagger\Client\Model\RoomTypeDTO.md)| JSON message with modified room type |

### Return type

[**\Swagger\Client\Model\ResponseWrapperDTORoomTypeDTO_**](../Model/ResponseWrapperDTORoomTypeDTO_.md)

### Authorization

[Basic](../../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/vnd.expedia.eps.product-v2+json
 - **Accept**: application/vnd.expedia.eps.product-v2+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

