# Swagger\Client\RoomTypeAmenitiesApi

All URIs are relative to *https://services.expediapartnercentral.com/products*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getRoomTypeAmenities**](RoomTypeAmenitiesApi.md#getRoomTypeAmenities) | **GET** /properties/{propertyId}/roomTypes/{roomTypeId}/amenities | Read a single room type&#39;s amenities
[**setRoomTypeAmenities**](RoomTypeAmenitiesApi.md#setRoomTypeAmenities) | **PUT** /properties/{propertyId}/roomTypes/{roomTypeId}/amenities | Set room type amenities to an existing room type


# **getRoomTypeAmenities**
> \Swagger\Client\Model\ResponseWrapperDTOListRoomTypeAmenityDTO_ getRoomTypeAmenities($property_id, $room_type_id)

Read a single room type's amenities

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');

$api_instance = new Swagger\Client\Api\RoomTypeAmenitiesApi();
$property_id = "property_id_example"; // string | Expedia Property ID
$room_type_id = "room_type_id_example"; // string | Room type resource ID. Integer

try {
    $result = $api_instance->getRoomTypeAmenities($property_id, $room_type_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoomTypeAmenitiesApi->getRoomTypeAmenities: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **string**| Expedia Property ID |
 **room_type_id** | **string**| Room type resource ID. Integer |

### Return type

[**\Swagger\Client\Model\ResponseWrapperDTOListRoomTypeAmenityDTO_**](../Model/ResponseWrapperDTOListRoomTypeAmenityDTO_.md)

### Authorization

[Basic](../../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.expedia.eps.product-v2+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setRoomTypeAmenities**
> \Swagger\Client\Model\ResponseWrapperDTOListRoomTypeAmenityDTO_ setRoomTypeAmenities($property_id, $room_type_id, $room_type_amenity_dt_os)

Set room type amenities to an existing room type

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');

$api_instance = new Swagger\Client\Api\RoomTypeAmenitiesApi();
$property_id = "property_id_example"; // string | Expedia Property Id
$room_type_id = "room_type_id_example"; // string | Room type resource ID
$room_type_amenity_dt_os = array(new RoomTypeAmenityDTO()); // \Swagger\Client\Model\RoomTypeAmenityDTO[] | JSON message with the room type amenities

try {
    $result = $api_instance->setRoomTypeAmenities($property_id, $room_type_id, $room_type_amenity_dt_os);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoomTypeAmenitiesApi->setRoomTypeAmenities: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **string**| Expedia Property Id |
 **room_type_id** | **string**| Room type resource ID |
 **room_type_amenity_dt_os** | [**\Swagger\Client\Model\RoomTypeAmenityDTO[]**](../Model/RoomTypeAmenityDTO.md)| JSON message with the room type amenities |

### Return type

[**\Swagger\Client\Model\ResponseWrapperDTOListRoomTypeAmenityDTO_**](../Model/ResponseWrapperDTOListRoomTypeAmenityDTO_.md)

### Authorization

[Basic](../../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/vnd.expedia.eps.product-v2+json
 - **Accept**: application/vnd.expedia.eps.product-v2+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

