# Swagger\Client\RatePlanApi

All URIs are relative to *https://services.expediapartnercentral.com/products*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createRatePlan**](RatePlanApi.md#createRatePlan) | **POST** /properties/{propertyId}/roomTypes/{roomTypeId}/ratePlans | Creates a new rate plan
[**deleteRatePlan**](RatePlanApi.md#deleteRatePlan) | **DELETE** /properties/{propertyId}/roomTypes/{roomTypeId}/ratePlans/{ratePlanId} | Delete an existing rate plan
[**getRatePlan**](RatePlanApi.md#getRatePlan) | **GET** /properties/{propertyId}/roomTypes/{roomTypeId}/ratePlans/{ratePlanId} | Read a single rate plan
[**getRatePlans**](RatePlanApi.md#getRatePlans) | **GET** /properties/{propertyId}/roomTypes/{roomTypeId}/ratePlans | Obtain a list of rate plans
[**patchRatePlan**](RatePlanApi.md#patchRatePlan) | **PATCH** /properties/{propertyId}/roomTypes/{roomTypeId}/ratePlans/{ratePlanId} | Patch an existing rate plan
[**updateRatePlan**](RatePlanApi.md#updateRatePlan) | **PUT** /properties/{propertyId}/roomTypes/{roomTypeId}/ratePlans/{ratePlanId} | Modify an existing rate plan


# **createRatePlan**
> \Swagger\Client\Model\ResponseWrapperDTORatePlanDTO_ createRatePlan($property_id, $room_type_id, $rate_plan_dto)

Creates a new rate plan

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');

$api_instance = new Swagger\Client\Api\RatePlanApi();
$property_id = "property_id_example"; // string | Expedia Property ID
$room_type_id = "room_type_id_example"; // string | Room type resource ID
$rate_plan_dto = new \Swagger\Client\Model\RatePlanDTO(); // \Swagger\Client\Model\RatePlanDTO | JSON message describing the new rate plan

try {
    $result = $api_instance->createRatePlan($property_id, $room_type_id, $rate_plan_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RatePlanApi->createRatePlan: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **string**| Expedia Property ID |
 **room_type_id** | **string**| Room type resource ID |
 **rate_plan_dto** | [**\Swagger\Client\Model\RatePlanDTO**](../Model/\Swagger\Client\Model\RatePlanDTO.md)| JSON message describing the new rate plan |

### Return type

[**\Swagger\Client\Model\ResponseWrapperDTORatePlanDTO_**](../Model/ResponseWrapperDTORatePlanDTO_.md)

### Authorization

[Basic](../../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/vnd.expedia.eps.product-v2+json
 - **Accept**: application/vnd.expedia.eps.product-v2+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteRatePlan**
> deleteRatePlan($property_id, $room_type_id, $rate_plan_id)

Delete an existing rate plan

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');

$api_instance = new Swagger\Client\Api\RatePlanApi();
$property_id = "property_id_example"; // string | Expedia Property ID
$room_type_id = "room_type_id_example"; // string | Room type resource ID
$rate_plan_id = "rate_plan_id_example"; // string | Rate plan resource ID

try {
    $api_instance->deleteRatePlan($property_id, $room_type_id, $rate_plan_id);
} catch (Exception $e) {
    echo 'Exception when calling RatePlanApi->deleteRatePlan: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **string**| Expedia Property ID |
 **room_type_id** | **string**| Room type resource ID |
 **rate_plan_id** | **string**| Rate plan resource ID |

### Return type

void (empty response body)

### Authorization

[Basic](../../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.expedia.eps.product-v2+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getRatePlan**
> \Swagger\Client\Model\ResponseWrapperDTORatePlanDTO_ getRatePlan($property_id, $room_type_id, $rate_plan_id)

Read a single rate plan

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');

$api_instance = new Swagger\Client\Api\RatePlanApi();
$property_id = "property_id_example"; // string | Expedia Property ID
$room_type_id = "room_type_id_example"; // string | Room type resource ID
$rate_plan_id = "rate_plan_id_example"; // string | Rate plan resource ID

try {
    $result = $api_instance->getRatePlan($property_id, $room_type_id, $rate_plan_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RatePlanApi->getRatePlan: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **string**| Expedia Property ID |
 **room_type_id** | **string**| Room type resource ID |
 **rate_plan_id** | **string**| Rate plan resource ID |

### Return type

[**\Swagger\Client\Model\ResponseWrapperDTORatePlanDTO_**](../Model/ResponseWrapperDTORatePlanDTO_.md)

### Authorization

[Basic](../../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.expedia.eps.product-v2+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getRatePlans**
> \Swagger\Client\Model\ResponseWrapperDTOListRatePlanDTO_ getRatePlans($property_id, $room_type_id, $status)

Obtain a list of rate plans

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');

$api_instance = new Swagger\Client\Api\RatePlanApi();
$property_id = "property_id_example"; // string | Expedia Property ID
$room_type_id = "room_type_id_example"; // string | Room type resource ID
$status = "status_example"; // string | Status filter. String. Only supported value is \"all\".

try {
    $result = $api_instance->getRatePlans($property_id, $room_type_id, $status);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RatePlanApi->getRatePlans: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **string**| Expedia Property ID |
 **room_type_id** | **string**| Room type resource ID |
 **status** | **string**| Status filter. String. Only supported value is \&quot;all\&quot;. | [optional]

### Return type

[**\Swagger\Client\Model\ResponseWrapperDTOListRatePlanDTO_**](../Model/ResponseWrapperDTOListRatePlanDTO_.md)

### Authorization

[Basic](../../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.expedia.eps.product-v2+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **patchRatePlan**
> \Swagger\Client\Model\ResponseWrapperDTORatePlanDTO_ patchRatePlan($property_id, $room_type_id, $rate_plan_id, $rate_plan)

Patch an existing rate plan

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');

$api_instance = new Swagger\Client\Api\RatePlanApi();
$property_id = "property_id_example"; // string | Expedia Property ID
$room_type_id = "room_type_id_example"; // string | Room type resource ID
$rate_plan_id = "rate_plan_id_example"; // string | Rate plan resource ID
$rate_plan = "rate_plan_example"; // string | JSON message of partially updated rate plan

try {
    $result = $api_instance->patchRatePlan($property_id, $room_type_id, $rate_plan_id, $rate_plan);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RatePlanApi->patchRatePlan: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **string**| Expedia Property ID |
 **room_type_id** | **string**| Room type resource ID |
 **rate_plan_id** | **string**| Rate plan resource ID |
 **rate_plan** | **string**| JSON message of partially updated rate plan |

### Return type

[**\Swagger\Client\Model\ResponseWrapperDTORatePlanDTO_**](../Model/ResponseWrapperDTORatePlanDTO_.md)

### Authorization

[Basic](../../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/vnd.expedia.eps.product-v2+json
 - **Accept**: application/vnd.expedia.eps.product-v2+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateRatePlan**
> \Swagger\Client\Model\ResponseWrapperDTORatePlanDTO_ updateRatePlan($property_id, $room_type_id, $rate_plan_id, $rate_plan)

Modify an existing rate plan

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');

$api_instance = new Swagger\Client\Api\RatePlanApi();
$property_id = "property_id_example"; // string | Expedia Property ID
$room_type_id = "room_type_id_example"; // string | Room type resource ID
$rate_plan_id = "rate_plan_id_example"; // string | Rate plan resource ID
$rate_plan = new \Swagger\Client\Model\RatePlanDTO(); // \Swagger\Client\Model\RatePlanDTO | JSON message of modified rate plan

try {
    $result = $api_instance->updateRatePlan($property_id, $room_type_id, $rate_plan_id, $rate_plan);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RatePlanApi->updateRatePlan: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **string**| Expedia Property ID |
 **room_type_id** | **string**| Room type resource ID |
 **rate_plan_id** | **string**| Rate plan resource ID |
 **rate_plan** | [**\Swagger\Client\Model\RatePlanDTO**](../Model/\Swagger\Client\Model\RatePlanDTO.md)| JSON message of modified rate plan |

### Return type

[**\Swagger\Client\Model\ResponseWrapperDTORatePlanDTO_**](../Model/ResponseWrapperDTORatePlanDTO_.md)

### Authorization

[Basic](../../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/vnd.expedia.eps.product-v2+json
 - **Accept**: application/vnd.expedia.eps.product-v2+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

