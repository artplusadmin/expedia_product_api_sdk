# Swagger\Client\PropertyApi

All URIs are relative to *https://services.expediapartnercentral.com/products*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getProperties**](PropertyApi.md#getProperties) | **GET** /properties | Obtain a list of properties
[**getProperty**](PropertyApi.md#getProperty) | **GET** /properties/{propertyId} | Read a single property


# **getProperties**
> \Swagger\Client\Model\ResponseWrapperDTOListPropertyDTO_ getProperties($status, $offset, $limit)

Obtain a list of properties

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');

$api_instance = new Swagger\Client\Api\PropertyApi();
$status = "status_example"; // string | Status filter. String. Only supported value is \"all\".
$offset = "0"; // string | Pagination offset. Integer starting at 0
$limit = "20"; // string | Pagination limit. Integer between 1 and 200.

try {
    $result = $api_instance->getProperties($status, $offset, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PropertyApi->getProperties: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **status** | **string**| Status filter. String. Only supported value is \&quot;all\&quot;. | [optional]
 **offset** | **string**| Pagination offset. Integer starting at 0 | [optional] [default to 0]
 **limit** | **string**| Pagination limit. Integer between 1 and 200. | [optional] [default to 20]

### Return type

[**\Swagger\Client\Model\ResponseWrapperDTOListPropertyDTO_**](../Model/ResponseWrapperDTOListPropertyDTO_.md)

### Authorization

[Basic](../../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.expedia.eps.product-v2+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProperty**
> \Swagger\Client\Model\ResponseWrapperDTOPropertyDTO_ getProperty($property_id)

Read a single property

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: Basic
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');

$api_instance = new Swagger\Client\Api\PropertyApi();
$property_id = "property_id_example"; // string | Expedia Property ID

try {
    $result = $api_instance->getProperty($property_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PropertyApi->getProperty: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **string**| Expedia Property ID |

### Return type

[**\Swagger\Client\Model\ResponseWrapperDTOPropertyDTO_**](../Model/ResponseWrapperDTOPropertyDTO_.md)

### Authorization

[Basic](../../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.expedia.eps.product-v2+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

