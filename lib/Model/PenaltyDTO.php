<?php
/**
 * PenaltyDTO
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Product API
 *
 * The Product API will enable Expedia lodging partners to read, create and edit room types and rate plans via APIs, without having to use EPC or contact their market manager.  </br><br/>To start experimenting, please use your existing EQC credentials and properties. We've also made the following test credentials available: EQCtest12933870 / ew67nk33 assigned to test property ID 12933870.
 *
 * OpenAPI spec version: 2.0
 * Contact: ProductAPI@expedia.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;

/**
 * PenaltyDTO Class Doc Comment
 *
 * @category    Class */
/** 
 * @package     Swagger\Client
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class PenaltyDTO implements ArrayAccess
{
    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'PenaltyDTO';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'amount' => 'double',
        'deadline' => 'int',
        'per_stay_fee' => 'string'
    );

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = array(
        'amount' => 'amount',
        'deadline' => 'deadline',
        'per_stay_fee' => 'perStayFee'
    );

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = array(
        'amount' => 'setAmount',
        'deadline' => 'setDeadline',
        'per_stay_fee' => 'setPerStayFee'
    );

    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = array(
        'amount' => 'getAmount',
        'deadline' => 'getDeadline',
        'per_stay_fee' => 'getPerStayFee'
    );

    public static function getters()
    {
        return self::$getters;
    }

    const PER_STAY_FEE_NONE = 'None';
    const PER_STAY_FEE__1ST_NIGHT_ROOM_AND_TAX = '1stNightRoomAndTax';
    const PER_STAY_FEE__2_NIGHTS_ROOM_AND_TAX = '2NightsRoomAndTax';
    const PER_STAY_FEE__10_PERCENT_COST_OF_STAY = '10PercentCostOfStay';
    const PER_STAY_FEE__20_PERCENT_COST_OF_STAY = '20PercentCostOfStay';
    const PER_STAY_FEE__30_PERCENT_COST_OF_STAY = '30PercentCostOfStay';
    const PER_STAY_FEE__40_PERCENT_COST_OF_STAY = '40PercentCostOfStay';
    const PER_STAY_FEE__50_PERCENT_COST_OF_STAY = '50PercentCostOfStay';
    const PER_STAY_FEE__60_PERCENT_COST_OF_STAY = '60PercentCostOfStay';
    const PER_STAY_FEE__70_PERCENT_COST_OF_STAY = '70PercentCostOfStay';
    const PER_STAY_FEE__80_PERCENT_COST_OF_STAY = '80PercentCostOfStay';
    const PER_STAY_FEE__90_PERCENT_COST_OF_STAY = '90PercentCostOfStay';
    const PER_STAY_FEE_FULL_COST_OF_STAY = 'FullCostOfStay';
    

    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getPerStayFeeAllowableValues()
    {
        return [
            self::PER_STAY_FEE_NONE,
            self::PER_STAY_FEE__1ST_NIGHT_ROOM_AND_TAX,
            self::PER_STAY_FEE__2_NIGHTS_ROOM_AND_TAX,
            self::PER_STAY_FEE__10_PERCENT_COST_OF_STAY,
            self::PER_STAY_FEE__20_PERCENT_COST_OF_STAY,
            self::PER_STAY_FEE__30_PERCENT_COST_OF_STAY,
            self::PER_STAY_FEE__40_PERCENT_COST_OF_STAY,
            self::PER_STAY_FEE__50_PERCENT_COST_OF_STAY,
            self::PER_STAY_FEE__60_PERCENT_COST_OF_STAY,
            self::PER_STAY_FEE__70_PERCENT_COST_OF_STAY,
            self::PER_STAY_FEE__80_PERCENT_COST_OF_STAY,
            self::PER_STAY_FEE__90_PERCENT_COST_OF_STAY,
            self::PER_STAY_FEE_FULL_COST_OF_STAY,
        ];
    }
    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = array();

    /**
     * Constructor
     * @param mixed[] $data Associated array of property value initalizing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['amount'] = isset($data['amount']) ? $data['amount'] : null;
        $this->container['deadline'] = isset($data['deadline']) ? $data['deadline'] : null;
        $this->container['per_stay_fee'] = isset($data['per_stay_fee']) ? $data['per_stay_fee'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = array();
        $allowed_values = array("None", "1stNightRoomAndTax", "2NightsRoomAndTax", "10PercentCostOfStay", "20PercentCostOfStay", "30PercentCostOfStay", "40PercentCostOfStay", "50PercentCostOfStay", "60PercentCostOfStay", "70PercentCostOfStay", "80PercentCostOfStay", "90PercentCostOfStay", "FullCostOfStay");
        if (!in_array($this->container['per_stay_fee'], $allowed_values)) {
            $invalid_properties[] = "invalid value for 'per_stay_fee', must be one of #{allowed_values}.";
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        $allowed_values = array("None", "1stNightRoomAndTax", "2NightsRoomAndTax", "10PercentCostOfStay", "20PercentCostOfStay", "30PercentCostOfStay", "40PercentCostOfStay", "50PercentCostOfStay", "60PercentCostOfStay", "70PercentCostOfStay", "80PercentCostOfStay", "90PercentCostOfStay", "FullCostOfStay");
        if (!in_array($this->container['per_stay_fee'], $allowed_values)) {
            return false;
        }
        return true;
    }


    /**
     * Gets amount
     * @return double
     */
    public function getAmount()
    {
        return $this->container['amount'];
    }

    /**
     * Sets amount
     * @param double $amount Min value 0.000 (3 decimal points). The amount provided here should be based on the property rate acquisition type. If the property rate acquisition type is Net, the rate provided here should be net of Expedia compensation. If it is SellLAR, the rate should be what the customer will be charged (inclusive of Expedia compensation). Used to define a flat amount that would be charged as a cancel or change penalty. This would normally replace a per-stay fee, but it can also be added on top of a per-stay fee if that is what the partner requires
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->container['amount'] = $amount;

        return $this;
    }

    /**
     * Gets deadline
     * @return int
     */
    public function getDeadline()
    {
        return $this->container['deadline'];
    }

    /**
     * Sets deadline
     * @param int $deadline Number of hours prior to the arrival of the guest. When set to 0, it means up until end of the day of arrival. Min 0, Max 999
     * @return $this
     */
    public function setDeadline($deadline)
    {
        $this->container['deadline'] = $deadline;

        return $this;
    }

    /**
     * Gets per_stay_fee
     * @return string
     */
    public function getPerStayFee()
    {
        return $this->container['per_stay_fee'];
    }

    /**
     * Sets per_stay_fee
     * @param string $per_stay_fee Fee that will be charged if the customer cancels within the specified deadline.
     * @return $this
     */
    public function setPerStayFee($per_stay_fee)
    {
        $allowed_values = array('None', '1stNightRoomAndTax', '2NightsRoomAndTax', '10PercentCostOfStay', '20PercentCostOfStay', '30PercentCostOfStay', '40PercentCostOfStay', '50PercentCostOfStay', '60PercentCostOfStay', '70PercentCostOfStay', '80PercentCostOfStay', '90PercentCostOfStay', 'FullCostOfStay');
        if (!in_array($per_stay_fee, $allowed_values)) {
            throw new \InvalidArgumentException("Invalid value for 'per_stay_fee', must be one of 'None', '1stNightRoomAndTax', '2NightsRoomAndTax', '10PercentCostOfStay', '20PercentCostOfStay', '30PercentCostOfStay', '40PercentCostOfStay', '50PercentCostOfStay', '60PercentCostOfStay', '70PercentCostOfStay', '80PercentCostOfStay', '90PercentCostOfStay', 'FullCostOfStay'");
        }
        $this->container['per_stay_fee'] = $per_stay_fee;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this));
    }
}


