<?php
/**
 * RoomTypeAmenitiesApi
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Product API
 *
 * The Product API will enable Expedia lodging partners to read, create and edit room types and rate plans via APIs, without having to use EPC or contact their market manager.  </br><br/>To start experimenting, please use your existing EQC credentials and properties. We've also made the following test credentials available: EQCtest12933870 / ew67nk33 assigned to test property ID 12933870.
 *
 * OpenAPI spec version: 2.0
 * Contact: ProductAPI@expedia.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Api;

use \Swagger\Client\Configuration;
use \Swagger\Client\ApiClient;
use \Swagger\Client\ApiException;
use \Swagger\Client\ObjectSerializer;

/**
 * RoomTypeAmenitiesApi Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class RoomTypeAmenitiesApi
{

    /**
     * API Client
     *
     * @var \Swagger\Client\ApiClient instance of the ApiClient
     */
    protected $apiClient;

    /**
     * Constructor
     *
     * @param \Swagger\Client\ApiClient|null $apiClient The api client to use
     */
    public function __construct(\Swagger\Client\ApiClient $apiClient = null)
    {
        if ($apiClient == null) {
            $apiClient = new ApiClient();
            $apiClient->getConfig()->setHost('https://services.expediapartnercentral.com/products');
        }

        $this->apiClient = $apiClient;
    }

    /**
     * Get API client
     *
     * @return \Swagger\Client\ApiClient get the API client
     */
    public function getApiClient()
    {
        return $this->apiClient;
    }

    /**
     * Set the API client
     *
     * @param \Swagger\Client\ApiClient $apiClient set the API client
     *
     * @return RoomTypeAmenitiesApi
     */
    public function setApiClient(\Swagger\Client\ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
        return $this;
    }

    /**
     * Operation getRoomTypeAmenities
     *
     * Read a single room type's amenities
     *
     * @param string $property_id Expedia Property ID (required)
     * @param string $room_type_id Room type resource ID. Integer (required)
     * @return \Swagger\Client\Model\ResponseWrapperDTOListRoomTypeAmenityDTO_
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function getRoomTypeAmenities($property_id, $room_type_id)
    {
        list($response) = $this->getRoomTypeAmenitiesWithHttpInfo($property_id, $room_type_id);
        return $response;
    }

    /**
     * Operation getRoomTypeAmenitiesWithHttpInfo
     *
     * Read a single room type's amenities
     *
     * @param string $property_id Expedia Property ID (required)
     * @param string $room_type_id Room type resource ID. Integer (required)
     * @return Array of \Swagger\Client\Model\ResponseWrapperDTOListRoomTypeAmenityDTO_, HTTP status code, HTTP response headers (array of strings)
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function getRoomTypeAmenitiesWithHttpInfo($property_id, $room_type_id)
    {
        // verify the required parameter 'property_id' is set
        if ($property_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $property_id when calling getRoomTypeAmenities');
        }
        // verify the required parameter 'room_type_id' is set
        if ($room_type_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $room_type_id when calling getRoomTypeAmenities');
        }
        // parse inputs
        $resourcePath = "/properties/{propertyId}/roomTypes/{roomTypeId}/amenities";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/vnd.expedia.eps.product-v2+json'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array('application/json'));

        // path params
        if ($property_id !== null) {
            $resourcePath = str_replace(
                "{" . "propertyId" . "}",
                $this->apiClient->getSerializer()->toPathValue($property_id),
                $resourcePath
            );
        }
        // path params
        if ($room_type_id !== null) {
            $resourcePath = str_replace(
                "{" . "roomTypeId" . "}",
                $this->apiClient->getSerializer()->toPathValue($room_type_id),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires HTTP basic authentication
        if (strlen($this->apiClient->getConfig()->getUsername()) !== 0 or strlen($this->apiClient->getConfig()->getPassword()) !== 0) {
            $headerParams['Authorization'] = 'Basic ' . base64_encode($this->apiClient->getConfig()->getUsername() . ":" . $this->apiClient->getConfig()->getPassword());
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\ResponseWrapperDTOListRoomTypeAmenityDTO_',
                '/properties/{propertyId}/roomTypes/{roomTypeId}/amenities'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\ResponseWrapperDTOListRoomTypeAmenityDTO_', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\ResponseWrapperDTOListRoomTypeAmenityDTO_', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation setRoomTypeAmenities
     *
     * Set room type amenities to an existing room type
     *
     * @param string $property_id Expedia Property Id (required)
     * @param string $room_type_id Room type resource ID (required)
     * @param \Swagger\Client\Model\RoomTypeAmenityDTO[] $room_type_amenity_dt_os JSON message with the room type amenities (required)
     * @return \Swagger\Client\Model\ResponseWrapperDTOListRoomTypeAmenityDTO_
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function setRoomTypeAmenities($property_id, $room_type_id, $room_type_amenity_dt_os)
    {
        list($response) = $this->setRoomTypeAmenitiesWithHttpInfo($property_id, $room_type_id, $room_type_amenity_dt_os);
        return $response;
    }

    /**
     * Operation setRoomTypeAmenitiesWithHttpInfo
     *
     * Set room type amenities to an existing room type
     *
     * @param string $property_id Expedia Property Id (required)
     * @param string $room_type_id Room type resource ID (required)
     * @param \Swagger\Client\Model\RoomTypeAmenityDTO[] $room_type_amenity_dt_os JSON message with the room type amenities (required)
     * @return Array of \Swagger\Client\Model\ResponseWrapperDTOListRoomTypeAmenityDTO_, HTTP status code, HTTP response headers (array of strings)
     * @throws \Swagger\Client\ApiException on non-2xx response
     */
    public function setRoomTypeAmenitiesWithHttpInfo($property_id, $room_type_id, $room_type_amenity_dt_os)
    {
        // verify the required parameter 'property_id' is set
        if ($property_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $property_id when calling setRoomTypeAmenities');
        }
        // verify the required parameter 'room_type_id' is set
        if ($room_type_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $room_type_id when calling setRoomTypeAmenities');
        }
        // verify the required parameter 'room_type_amenity_dt_os' is set
        if ($room_type_amenity_dt_os === null) {
            throw new \InvalidArgumentException('Missing the required parameter $room_type_amenity_dt_os when calling setRoomTypeAmenities');
        }
        // parse inputs
        $resourcePath = "/properties/{propertyId}/roomTypes/{roomTypeId}/amenities";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/vnd.expedia.eps.product-v2+json'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array('application/vnd.expedia.eps.product-v2+json'));

        // path params
        if ($property_id !== null) {
            $resourcePath = str_replace(
                "{" . "propertyId" . "}",
                $this->apiClient->getSerializer()->toPathValue($property_id),
                $resourcePath
            );
        }
        // path params
        if ($room_type_id !== null) {
            $resourcePath = str_replace(
                "{" . "roomTypeId" . "}",
                $this->apiClient->getSerializer()->toPathValue($room_type_id),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        // body params
        $_tempBody = null;
        if (isset($room_type_amenity_dt_os)) {
            $_tempBody = $room_type_amenity_dt_os;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires HTTP basic authentication
        if (strlen($this->apiClient->getConfig()->getUsername()) !== 0 or strlen($this->apiClient->getConfig()->getPassword()) !== 0) {
            $headerParams['Authorization'] = 'Basic ' . base64_encode($this->apiClient->getConfig()->getUsername() . ":" . $this->apiClient->getConfig()->getPassword());
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'PUT',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\ResponseWrapperDTOListRoomTypeAmenityDTO_',
                '/properties/{propertyId}/roomTypes/{roomTypeId}/amenities'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\ResponseWrapperDTOListRoomTypeAmenityDTO_', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\ResponseWrapperDTOListRoomTypeAmenityDTO_', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

}
